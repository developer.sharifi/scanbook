from scanbook_server.core.db_handler import DBHandler
from scanbook_server.core.domain_models.book import Book


class BookUseCase:


    def create_book(self, fidibo_id:int, ISBN:str, title:str, category:str, rating:float, release_date:str,
                 publisher:str, pages:int, picture_url:str, description:str):
        db_handler = DBHandler()
        db_book = db_handler.create_book(fidibo_id=fidibo_id, ISBN=ISBN, title=title, category=category,
                       rating=rating, release_date=release_date, publisher=publisher,
                       pages=pages, picture_url=picture_url, description=description)

        domain_book = Book(id=db_book.id, fidibo_id=db_book.fidibo_id, ISBN=db_book.ISBN,
                           title=db_book.title, category=db_book.category, rating=db_book.rating,
                           release_date=db_book.release_date, publisher=db_book.publisher,
                           pages=db_book.pages, picture_url=db_book.picture_url,
                           description=db_book.description)

        db_handler.session.close()
        return domain_book


    def get_book_by_ISBN(self, ISBN:str):
        db_handler = DBHandler()
        db_book = db_handler.get_book_by_ISBN(ISBN=ISBN)
        if not db_book:
            return None
        domain_book = Book(id=db_book.id, fidibo_id=db_book.fidibo_id, ISBN=db_book.ISBN,
                           title=db_book.title, category=db_book.category, rating=db_book.rating,
                           release_date=db_book.release_date, publisher=db_book.publisher,
                           pages=db_book.pages, picture_url=db_book.picture_url,
                           description=db_book.description)

        db_handler.session.close()

        return domain_book


    def get_book_by_id(self, id:int):
        db_handler = DBHandler()
        db_book = db_handler.get_book_by_id(id=id)

        if not db_book:
            return None

        domain_book = Book(id=db_book.id, fidibo_id=db_book.fidibo_id, ISBN=db_book.ISBN,
                           title=db_book.title, category=db_book.category, rating=db_book.rating,
                           release_date=db_book.release_date, publisher=db_book.publisher,
                           pages=db_book.pages, picture_url=db_book.picture_url,
                           description=db_book.description)

        db_handler.session.close()

        return domain_book


    def search_books_by_title(self, title:str):
        db_handler = DBHandler()
        db_books = db_handler.search_books_by_title(title=title)

        domain_books = []
        for a_db_book in db_books:
            a_domain_book = Book(id=a_db_book.id, fidibo_id=a_db_book.fidibo_id, ISBN=a_db_book.ISBN,
                           title=a_db_book.title, category=a_db_book.category, rating=a_db_book.rating,
                           release_date=a_db_book.release_date, publisher=a_db_book.publisher,
                           pages=a_db_book.pages, picture_url=a_db_book.picture_url,
                           description=a_db_book.description)
            domain_books.append(a_domain_book)

        return domain_books