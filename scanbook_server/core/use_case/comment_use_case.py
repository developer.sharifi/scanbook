from scanbook_server.core.db_handler import DBHandler
from scanbook_server.core.domain_models.comment import Comment


class CommentUseCase:

    def create_comment(self, book_id:int, sender:str, date:str, content:str):
        db_handler = DBHandler()
        db_comment = db_handler.create_comment(book_id=book_id, sender=sender,
                                               date=date, content=content)

        domain_comment = Comment(book_id=db_comment.book_id, sender=db_comment.sender,
                                 date=db_comment.sender, content=db_comment.content)

        db_handler.session.close()
        return domain_comment
