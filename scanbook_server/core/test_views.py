# from scanbook_server.core import views
# import unittest
# from django.test import RequestFactory
#
# class SearchBooksAPIViewTestCase(unittest.TestCase):
#
#     def setUp(self):
#         request_factory = RequestFactory()
#         self.request1 = request_factory.get(path='/fake-path')
#         self.request2 = request_factory.get(path='/fake-path', data={'title': 'قانون'})
#         self.request3 = request_factory.get(path='/fake-path', data={'title': 'jabbarsing'})
#         self.view = views.SearchBooksAPIView.as_view()
#         self.response1 = self.view(self.request1)
#         self.response2 = self.view(self.request2)
#         self.response3 = self.view(self.request3)
#
#     def test_get(self):
#         self.assertEqual(self.response1.data.get('error'), 'title is required!')
#         self.assertEqual(self.response1.status_code, 400)
#
#
# if __name__=='__main__':
#     unittest.main()