from django.urls import path

from scanbook_server.core.views import BookDetailsAPIView, CommentListAPIView, InitDatabaseAPIView, SearchBooksAPIView

urlpatterns = [
    path("books", SearchBooksAPIView.as_view()),
    path("books/<str:ISBN>", BookDetailsAPIView().as_view()),
    path("comments", CommentListAPIView().as_view()),
    path("fill-database", InitDatabaseAPIView.as_view())
]
