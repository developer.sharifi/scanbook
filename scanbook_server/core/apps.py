from django.apps import AppConfig


class ScanbookConfig(AppConfig):
    name = 'scanbook_server.core'
