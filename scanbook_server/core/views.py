# from django.shortcuts import render

# from scanbook_server.core.use_case.comment_use_case import CommentUseCase
from scanbook_server.core.use_case.db_filler import StoreBookToDBThread
from scanbook_server.crawlers import crawler

# Create your views here.
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from scanbook_server.core.serializers import GetBookDetailsSerializer, GetCommentSerializer, GetBookSummarySerializer
from scanbook_server.core.use_case.book_use_case import BookUseCase
import threading
import queue

class BookDetailsAPIView(APIView):
    def get(self, request, ISBN, *args, **kwargs):
        print('ISBN:', ISBN)
        book_use_case = BookUseCase()
        domain_book = book_use_case.get_book_by_ISBN(ISBN=ISBN)
        if not domain_book:
            return Response(data={"error": "book not found!"}, status=status.HTTP_404_NOT_FOUND)

        serializer = GetBookDetailsSerializer(instance=domain_book)
        return Response(data=serializer.data, status=status.HTTP_200_OK)


class CommentListAPIView(APIView):
    def get(self, request, *args, **kwargs):
        book_id = request.data.get('book_id')
        print('request.data:', request.data)
        if not book_id:
            return Response(data={"error": "book_id is required!"}, status=status.HTTP_400_BAD_REQUEST)

        book_use_case = BookUseCase()
        domain_book = book_use_case.get_book_by_id(id=book_id)
        if not domain_book:
            return Response(data={"error": "book not found!"}, status=status.HTTP_404_NOT_FOUND)

        domain_comments = domain_book.get_comments()
        serializer = GetCommentSerializer(instance=domain_comments, many=True)
        return Response(data=serializer.data, status=status.HTTP_200_OK)


class InitDatabaseAPIView(APIView):
    workQueue = queue.Queue(1000)
    queueLock = threading.Lock()

    def post(self, request, *args, **kwargs):
        cat = request.data.get('category')
        if not cat:
            return Response(data={"error": "category is required!"}, status=status.HTTP_400_BAD_REQUEST)

        book_page_urls = crawler.get_fidibo_page_books_urls(cat=cat)
        for url in book_page_urls:
            self.workQueue.put(url)
            print(url)
            t = StoreBookToDBThread(self.workQueue, self.queueLock)
            t.start()

        return Response(data={"result": "Database filled :)"}, status=status.HTTP_201_CREATED)


class SearchBooksAPIView(APIView):
    def get(self, request, *args, **kwargs):
        title = request.data.get('title')
        if not title:
            return Response(data={"error": "title is required!"}, status=status.HTTP_400_BAD_REQUEST)
        book_use_case = BookUseCase()
        books = book_use_case.search_books_by_title(title=str(title))
        serializer = GetBookSummarySerializer(instance=books, many=True)
        response_data = {'count': books.__len__(), 'books': serializer.data}
        return Response(data=response_data, status=status.HTTP_200_OK)