from scanbook_server.core.db_handler import DBHandler
from scanbook_server.core.domain_models.comment import Comment


class Book:

    def __init__(self, id:int, fidibo_id:int, ISBN:str, title:str, category:str, rating:float, release_date:str,
                 publisher:str, pages:int, picture_url:str, description:str):
        self.id = id
        self.fidibo_id = fidibo_id
        self.ISBN = ISBN
        self.title = title
        self.category = category
        self.rating = rating
        self.release_date = release_date
        self.publisher = publisher
        self.pages = pages
        self.picture_url=picture_url
        self.description = description

    def get_comments(self):
        db_handler = DBHandler()
        db_comments = db_handler.get_book_comments_by_book_id(book_id=self.id)

        domain_comments = []
        for a_db_comment in db_comments:
            a_domain_comment = Comment(book_id=a_db_comment.book_id, sender=a_db_comment.sender,
                                       date=a_db_comment.date, content=a_db_comment.content)
            domain_comments.append(a_db_comment)

        db_handler.session.close()

        return domain_comments

