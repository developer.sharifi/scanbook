import unittest
from scanbook_server.crawlers import crawler

class TestCrawler(unittest.TestCase):
    def test_get_fidibo_book_details(self):

        result = crawler.get_fidibo_book_details(url='https://fidibo.com/book/79622-%DA%A9%D8%AA%D8%A7%D8%A8-%D9%87%D9%81%D8%AA-%D8%B9%D8%A7%D8%AF%D8%AA-%D9%85%D8%B1%D8%AF%D9%85%D8%A7%D9%86-%D9%85%D9%88%D8%AB%D8%B1');

        self.assertEqual(result.get('title'), 'کتاب هفت عادت مردمان مؤثر')
        self.assertEqual(result.get('category'), 'روانشناسی')
        self.assertEqual(result.get('rating'), 3.84)
        self.assertEqual(result.get('release_date'), '۱۳۹۶/۱۰/۱۵')
        self.assertEqual(result.get('publisher'), 'انتشارات هامون')
        self.assertEqual(result.get('pages'), 348)
        # self.assertEqual(result.get('description').split(), "عادت")
        self.assertEqual(result.get('ISBN'), '964-91466-8-7')
        self.assertEqual(result.get('fidibo_id'), 79622)
        self.assertEqual(result.get('picture_url'), 'https://cdn.fidibo.com/images/books/79622_38212_normal.jpg')
        self.assertIsNotNone(result.get('comments'))

if __name__ == '__main__':
    unittest.main()