from bs4 import BeautifulSoup
import requests
import re


def get_fidibo_book_details(url: str):
    book = {}

    # finding title
    try:
        page = requests.get(url=url)
        soup = BeautifulSoup(page.text)
        title_tag = soup.find(name='h1', attrs={"style": "color: #404040;"})
        title = re.sub(' +', ' ', title_tag.text)
        title = title.strip()
        book['title'] = title
    except AttributeError:
        return None

    # finding category
    try:
        category_tag = soup.select_one("a[href*=category]")
        category_soup = BeautifulSoup(str(category_tag))
        category = category_soup.find(name='span').text
        book['category'] = category
    except AttributeError:
        return None

    # finding rating
    try:
        rating_tag = soup.find(name='input', attrs={"id": "book-rank"})
        rating = float(rating_tag.get('value'))
        book['rating'] = rating
    except AttributeError as e:
        book['rating'] = 0

    # finding release_date
    release_date_tag = soup.find(name='time')
    release_date = release_date_tag.text
    book['release_date'] = release_date

    # finding publisher
    publisher_tag = soup.select_one("a[href*=publisher]")
    publisher = publisher_tag.text
    book['publisher'] = publisher

    # finding pages
    try:
        pages_tag = soup.find_all(text=re.compile("([^آز])صفحه([^آز])"))
        pages_text = pages_tag[0].strip()
        pages = int(pages_text.split(' ')[0])
        book['pages'] = pages
    except (IndexError, ValueError):
        book['pages'] = 0

    # finding ISBN
    try:
        ISBN_tag = soup.find(name='label', attrs={"style": "direction: ltr !important;"})
        ISBN = ISBN_tag.text
        ISBN = ISBN.replace('-', '')
        book['ISBN'] = ISBN
    except AttributeError as e:
        return None
        # book['ISBN'] = '0'


    # fidibo_id
    fidibo_id = int((url.split('book/')[1]).split('-')[0])
    book['fidibo_id'] = fidibo_id

    # finding description
    try:
        description_tag = soup.find(name='p', attrs={"class": "more-info book-description"})
        description = description_tag.text.strip()
        book['description'] = description
    except AttributeError:
        return None

    # finding picture url
    picture_tag = soup.find(name='img', attrs={'id': 'book_img'})
    picture_url = picture_tag.get('src')
    book['picture_url'] = picture_url

    # finding comments
    comment_box_list = soup.find_all(name='div', attrs={'class': 'comment-box'})

    comments = []
    for comment_box in comment_box_list:
        sender = comment_box.find(name='span', attrs={"class": "sender"}).text.strip().replace('خریدار این کتاب','')
        date = comment_box.find(name='span', attrs={"class": "comment-date"}).text.strip()
        content = comment_box.find(name='div', attrs={"class": "comment-content"}).text.strip()
        a_comment = {'sender': sender, 'date': date, 'content': content}
        comments.append(a_comment)
    book['comments'] = comments

    return book



def get_fidibo_page_books_urls(cat: str):
    book_urls = []
    page_number = 1
    page_available = True
    while page_available:
        url = 'https://fidibo.com/category/{}?page={}&keyword=&sorting='.format(cat, page_number)
        page = requests.get(url=url)
        if page.status_code == 404:
            page_available = False
        else:
            soup = BeautifulSoup(page.text)
            book_boxes = soup.find_all(name='div', attrs={"class": "col-lg-2 col-md-3 col-sm-4 col-xs-6"})
            for box in book_boxes:
                a_tag = box.find(name='a', attrs={'itemprop':'url'})
                this_book_url = 'https://fidibo.com' + a_tag.get('href')
                book_urls.append(this_book_url)

            page_number += 1

    return book_urls